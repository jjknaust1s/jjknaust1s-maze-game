const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
const mazeEl = document.getElementById('maze')
for (let rowNum = 0; rowNum < map.length; rowNum++) {
    const rowString = map[rowNum]
    let blockDivs = ''

    for (colNum = 0; colNum < rowString.length; colNum++) {
        const blockType = rowString[colNum]
        if (blockType === 'W') {
            blockDivs += '<div class="block wall"></div>'
        } else {
            blockDivs += '<div class="block"></div>'
        }

        mazeEl.innerHTML += `<div class="row">${blockDivs}</div>`
    }
}