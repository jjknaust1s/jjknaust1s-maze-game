https://jjknaust1s.gitlab.io/jjknaust1s-maze-game 
1.) Draw a maze
    a.) make each cell a div
    b.) make each row maze display flex
2.) Allow user to move around the maze and but not move through walls
    a.) use an absolutely-positioned DIV to represent the player's current position in the maze, or 
    b.) have your player DIV appended to a cell DIV for the same reason.

<!-- look at listening to keyboards assessment -->
3.) Winning Aler when User reaches finsih
    a.) when player is at set location make and alert saying you win
RUBRIC
<!-- 1.) Maze and player are displayed on the page, and the player starts on the start square.
3.0 pts -->
2.)Player can be moved through the maze by arrow keys.
3.0 pts
<!-- 3.)Player cannot move onto walls or outside of the maze.
3.0 pts -->
4.)User is notified once the player reaches the finish square (no alert or console.log).
1.0 pts